package officeolympics



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ResultController {

	def gold;
	def silver;
	def bronze;
	def goldWinner;
	def silverWinner;
	def bronzeWinner;
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Result.list(params), model:[resultInstanceCount: Result.count()]
    }

    def show(Result resultInstance) {
        respond resultInstance
    }

    def create() {
        respond new Result(params)
    }

    @Transactional
    def save(Result resultInstance) {
        if (resultInstance == null) {
            notFound()
            return
        }

        if (resultInstance.hasErrors()) {
            respond resultInstance.errors, view: 'create'
            return
        }

        gold = new Medals();
        gold.sportName = resultInstance.sportName;
        gold.level = "Gold";
        goldWinner = resultInstance.goldWinner
        gold.teams = goldWinner
        Team.findById(goldWinner.id).medals.add(gold)
        Team.findById(goldWinner.id).score += 3;
        gold.save flush: true

        silver = new Medals();
        silver.sportName = resultInstance.sportName;
        silver.level = "Silver";
        silverWinner = resultInstance.silverWinner
        silver.teams = silverWinner
        Team.findById(silverWinner.id).medals.add(silver)
        Team.findById(silverWinner.id).score += 2
        silver.save flush: true

        bronze = new Medals();
        bronze.sportName = resultInstance.sportName;
        bronze.level = "Bronze";
        bronzeWinner = resultInstance.bronzeWinner
        bronze.teams = bronzeWinner
        Team.findById(bronzeWinner.id).medals.add(bronze)
        Team.findById(bronzeWinner.id).score += 1
        bronze.save flush: true

        redirect(controller: 'team', view: 'index')
    }

    def edit(Result resultInstance) {
        respond resultInstance
    }

    @Transactional
    def update(Result resultInstance) {
        if (resultInstance == null) {
            notFound()
            return
        }

        if (resultInstance.hasErrors()) {
            respond resultInstance.errors, view:'edit'
            return
        }

        resultInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Result.label', default: 'Result'), resultInstance.id])
                redirect resultInstance
            }
            '*'{ respond resultInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Result resultInstance) {

        if (resultInstance == null) {
            notFound()
            return
        }

        resultInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Result.label', default: 'Result'), resultInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'result.label', default: 'Result'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
