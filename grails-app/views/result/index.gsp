
<%@ page import="officeolympics.Result" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'result.label', default: 'Result')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-result" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-result" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="bronzeWinner" title="${message(code: 'result.bronzeWinner.label', default: 'Bronze Winner')}" />
					
						<g:sortableColumn property="goldWinner" title="${message(code: 'result.goldWinner.label', default: 'Gold Winner')}" />
					
						<g:sortableColumn property="silverWinner" title="${message(code: 'result.silverWinner.label', default: 'Silver Winner')}" />
					
						<g:sortableColumn property="sportName" title="${message(code: 'result.sportName.label', default: 'Sport Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${resultInstanceList}" status="i" var="resultInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${resultInstance.id}">${fieldValue(bean: resultInstance, field: "bronzeWinner")}</g:link></td>
					
						<td>${fieldValue(bean: resultInstance, field: "goldWinner")}</td>
					
						<td>${fieldValue(bean: resultInstance, field: "silverWinner")}</td>
					
						<td>${fieldValue(bean: resultInstance, field: "sportName")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${resultInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
