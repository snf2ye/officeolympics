%{--<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Grails</title>
		<style type="text/css" media="screen">
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body>
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="status" role="complementary">
			<h1>Application Status</h1>
			<ul>
				<li>App version: <g:meta name="app.version"/></li>
				<li>Grails version: <g:meta name="app.grails.version"/></li>
				<li>Groovy version: ${GroovySystem.getVersion()}</li>
				<li>JVM version: ${System.getProperty('java.version')}</li>
				<li>Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</li>
				<li>Controllers: ${grailsApplication.controllerClasses.size()}</li>
				<li>Domains: ${grailsApplication.domainClasses.size()}</li>
				<li>Services: ${grailsApplication.serviceClasses.size()}</li>
				<li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>
			</ul>
			<h1>Installed Plugins</h1>
			<ul>
				<g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">
					<li>${plugin.name} - ${plugin.version}</li>
				</g:each>
			</ul>
		</div>
		<div id="page-body" role="main">
			<h1>Welcome to Grails</h1>
			<p>Congratulations, you have successfully started your first Grails application! At the moment
			   this is the default page, feel free to modify it to either redirect to a controller or display whatever
			   content you may choose. Below is a list of controllers that are currently deployed in this application,
			   click on each to execute its default action:</p>

			<div id="controller-list" role="navigation">
				<h2>Available Controllers:</h2>
				<ul>
					<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
						<li class="controller"><g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link></li>
					</g:each>
				</ul>
			</div>
		</div>
	</body>
</html>
--}%

<%--
  Created by IntelliJ IDEA.
  User: jlee288
  Date: 7/14/2015
  Time: 12:58 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<neta charset="utf-8" />
	<title>CSC Office Olympic</title>
	<meta name="viewport" content="width=divice-width, initial-scale=1.0" >
	<style>
		body{
			background: #DAD5D5;
			font-family: Verdana, Tahoma, Arial, sans-serif;
			font-size: 18px;
			overflow: auto;
		}
		h1, h2, h3 {
			text-align: center;
			padding-left: 5%;
			color: #EC0707;
		}
		p{
			padding:2%;
			color: #EC0707;
		}

		img{
			text-align: center;
			max-width: 100%;
			height: auto;
			width: auto;
		}
		#wrapper{
			margin: 0 auto;
			max-width: 1020px;
			width:98%;
			background:#FFFFFF;
			border:1px solid #EC0707;
			border-radius:2px;
			box-shadow: 0 0 10px 0px rgba(12, 3, 25, 0.8);
		}

		#slogan{
			width:100%;
			height:auto;
			background:#EC0707;
			overflow:hidden;
		}

		#slogan p {
			text-align: right;
			color:#FFFFFF;
			font-size: 13px;
			padding: 0.1% 5% 0 0;
		}

		#slogan p a {
			color:#FFFFFF;
			text-decoration: none;
		}
		header{
			width:96%
			min-height: 125px;
			padding: 5px;
			text-align: center;
		}
		nav ul{
			list-style:none;
			margin: 0;
			padding-left:250px;
		}
		nav ul li{
			float:left;
			border: 1px solid #EC0707;
			width: 15%;
		}
		nav ul li a{
			background:#FFFFFF;
			display: block;
			padding: 5% 12%;
			font-weight: bold;
			font-size: 18px;
			color: #EC0707;
			text-decoration: none;
			text-align: center;
		}
		nav ul li a:hover, nav ul li.active a {
			background-color:#EC0707;
			color:#FFFFFF;
		}
		.clearfix{
			clear:both;
		}
		.banner img{
			width: 100%;
			border-top: 1px solid #EC0707;
			border-bottom: 1px solid #EC0707;
		}
		.left-col {
			width: 55%;
			float: left;
			margin: -2% 1% 1% 1%;
		}
		.sidebar{
			width: 40%;
			float: right;
			margin: 1%;
			text-align: center;
		}
		.rightpicture{
			float:left;
			margin: 0 auto;
			width: 100%;
			height: auto;
			padding: 1%;
		}
		.section{
			width:29%;
			float:left;
			margin: 2%;
			text-align: center;
		}
		footer{
			background: #EC0707;
			width: 100%;
			overflow: hidden;
		}
		footer p, footer h3 {
			color:#FFFFFF;
		}
		footer p a{
			color:#FFFFFF;
			text-decoration: none;
		}
		ul{
			list-style-type: none;
			margin: 0;
			padding: 0;
		}
		li{
			display: inline;
		}
		ul li img{
			height: 50px;
		}

		#slider {
			width:720px;
			height:400px;
			overflow:hidden;
		}
		#slider .slides{
			display: black;
			width:6000px;
			height: 400px;
			margin:0;
			padding:0;
		}
		#slider .slide{
			float:left;
			list-style-type: none;
			width:720px;
			height:400px;
		}

		/*---------------------MEDIA! ---------------------------*/
		@media sreen and (max-width: 478px){
			body{
				font-size:13px;
			}
		}
		@media sreen and (max-width: 740px){
			nav {
				width: 100%;
				margin-bottom: 10px;
			}
			nav ul {
				list-style: none;
				margin: 0 auto;
				padding-left:0;
			}
			nav ul li{
				text-align:center;
				margin-left: 0 auto;
				width:100%;
				border-top: 1px solid #EC0707;
				border-right: 0px solid #EC0707;
				border-left: 0px solid #EC0707;
				border-bottom: 1px solid #EC0707;
			}
			nav ul li a {
				padding: 8px 0;
				font-size: 16px;
			}
		}
	</style>
	<link rel="stylesheet" href="jquery-ui.min.css">
	<script src="external/jquery/jquery.js"></script>
	<script src="jquery-ui.min.js"></script>
<script type="text/javascript">
 $(function(){
	var width = 720;
	 var animationSpeed = 1000;
	 var pause = 3000;
	 var currentSlide = 1;

	 var $slider = $('#slider');
	 var $slideContainer = $slider.find('.slides');
	 var $slides= $slideContainer.find('.slide');

	 var interval;
	 function startSlider() {
		 interval = setInterval(function () {
			 $slideContainer.animate({'margin-left': '-=' + width}, animationSpeed, function () {
				 currentSlide++;
				 if (currentSlide === $slides.length) {
					 currentSlide = 1;
					 $slideContainer.css('margin-left', 0);
				 }
			 });
		 }, pause);
	 }
	 function stopSlider(){
		 clearInterval(interval);
	 }
	 $slider.on('mouseenter', stopSlider).on('mouseleave', startSlider);
	 startSlider();
 })
</script>


</head>

<body>
	<div id="wrapper">
		<div id="slogan">
			<p><b>CSC Office Olympics</b></p>
		</div>
		<header>
			<a href="#"><img src="http://www.investlithuania.com/wp-content/uploads/2014/06/CSC-logo_Lithuania.png" alt="mylogo" title="mylogo"/></a>
		</header>
		<nav>
			<ul>
				<li><a href="#">Home</a></li>
				<li><g:link controller="Team">Team</g:link></li>
				<li><g:link controller="TeamMembers">Members</g:link></li>
				<li><g:link controller="Medals">Medals</g:link></li>
			</ul>
			<div class="clearfix"></div>
		</nav>
		<div class="banner">
			<img src="https://s-media-cache-ak0.pinimg.com/736x/79/9e/b9/799eb92be38c5fcc5cc9d703f0831e54.jpg"/>
		</div>

		<div id="slider">
			<ul class="slides">
				<li class="slide"><img src="http://lifeintheoffice.com/wp-content/uploads/2006/06/flonkerton.png"/></li>
				<li class="slide"><img src="http://chroniclexmedia.com/wp-content/uploads/The-Office-Olympics-Screen-Cap-900-600.jpg"/></li>
				<li class="slide"><img src="http://info.mycccu.com/CustomImagelib/Chri19362fGamePhoto-web.jpg"/></li>
				<li class="slide"><img src="http://olympics.lms.kent.edu/logos/Office%20Olympics%20emblem_small.jpg"/></li>
				<li class="slide"><img src="http://www.bullionstreet.com/uploads/news/2012/6/1339140740.jpg"/></li>
			</ul>
		</div>

		<section class="left-col">
			<p style="text-indent: 50px;">some random text here</p>
			<p style="text-indent: 50px;">another random text here</p>
		</section>

		<aside class="sidebar">
			<div class="rightpicture">
				<img src="http://i.ytimg.com/vi/g-X0V_f9p-E/hqdefault.jpg"/>
			</div>
		</aside>
		<div class="clearfix"></div>
		<div class="section">
			<h3>Heading</h3>
			<img src=""/>
			<p>some text</p>
		</div>

		<div class="section">
			<h3>Heading</h3>
			<img src=""/>
			<p>some text</p>
		</div>

		<div class="section">
			<h3>Heading</h3>
			<img src=""/>
			<p>some text</p>
		</div>


		<footer>
			<div class="section">
				<p>CSC Dream Team</p>
				<p><b>3170 Fairview Park Drive<br>
				Falls Church, VA 22042<br>
				www.csc.com</b></p>
			</div>

			<div class="section">
				<p>Connect With Us!</p>
				<ul>
					<li><a href="#"><img src="http://www.socanews.com/_images/logos/twitter-icon.png"/></a></li>
					<li><a href="#"><img src="http://photos.state.gov/libraries/sample/788/social-media/60x60_facebook.gif"/></a></li>
					<li><a href="#"><img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRtnkXnhTV9qMEKpuWKAhuQ1zMs3DGLC2Dyb7tGsueLZk471uwo"/></a></li>
				</ul>
			</div>

			<div class="section">
				<img src="http://www.insyncconsulting.com/wp-content/themes/InSyncTheme/images/clients/CSC.png"/>
			</div>
		</footer>
	</div>
		<p style="text-align: center; padding: 0px;">&#169;Copyright - CSC dream team Office Olympics, 2015</p>

</body>
</html>